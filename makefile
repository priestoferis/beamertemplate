tex = lualatex
bib = biber
date = $(shell date --iso=date)
texfile = main
bibfile = ref.bib
#~ current_dir := $(shell ${PWD##*/})

all: $(texfile).bbl presentation.pdf handout.pdf notes.pdf

refs:
	$(tex) $(texfile).tex
	$(bib) $(texfile)

presentation.pdf: $(texfile).bbl $(texfile).tex
	$(tex) $(texfile).tex
	mkdir -p versions
	cp $(texfile).pdf presentation.pdf
	cp $(texfile).pdfpc presentation.pdfpc
	cp $(texfile).pdf versions/presentation_$(date).pdf
	cp $(texfile).pdfpc versions/presentation_$(date).pdfpc
	#~ cp $(texfile).pdf cur_dir.pdf

	
handout.pdf: $(texfile).bbl $(texfile).tex
	$(tex) "\def\ishandoutversion{1} \input{$(texfile).tex}"
	cp $(texfile).pdf handout.pdf
	cp $(texfile).pdf versions/handout_$(date).pdf
	
notes.pdf: $(texfile).bbl $(texfile).tex
	$(tex) "\def\isnotesversion{1} \input{$(texfile).tex}"
	cp $(texfile).pdf notes.pdf
	cp $(texfile).pdf versions/notes_$(date).pdf
	rm main.pdf
	rm main.pdfpc

$(texfile).bbl: $(bibfile)
	$(tex) $(texfile).tex
	$(bib) $(texfile)

	
clear:
	rm *.aux *.xml *.log *.bbl *.bcf *.blg *.out *.snm *.toc *.nav


