\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fbence_presentation}[2013/12/18 Ferdinandy Bence presentation class]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions \relax
\LoadClass[13pt,professionalfonts]{beamer}

\RequirePackage{pdfpcnotes}
%TODO: fix pdfpcnotes
%math: the ams package provide characters and symbols
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage[tight,nice]{units} %for typesetting units of measurements in a nice way (also loads nicefrac)
\RequirePackage{esdiff} %derivatives and partial derivatives in an easy way

%units
\RequirePackage{units}

%nicer squareroot
\RequirePackage{letltxmacro}

\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2}}


%fonts
\RequirePackage[no-math]{fontspec} %this is fontspec, but can set math stuff as well
\usefonttheme[onlymath]{serif}
\setsansfont{Source Sans Pro}

\newfontfamily\fontel[]{Source Sans Pro ExtraLight}
\newfontfamily\fontl[]{Source Sans Pro Light}
\newfontfamily\fontr[]{Source Sans Pro}
\newfontfamily\fontcr[]{Source Code Pro}
\newfontfamily\fontcl[]{Source Code Pro Light}
\newfontfamily\fontcel[]{Source Code Pro ExtraLight}

%language
\RequirePackage{polyglossia}
%\RequirePackage{csquotes}
%
\RequirePackage{enumerate} %allows a variaty of enumerations with optional arguments

%citations
\RequirePackage[]{biblatex}

\DeclareCiteCommand{\citepres}{\usebibmacro{prenote}}%
{\printfield{labeltitle} --- \printnames{labelname}%
\addspace\printfield{year}}%
{\multicitedelim}{\usebibmacro{postnote}}

\DeclareCiteCommand{\citeprest}{\usebibmacro{prenote}}%
{\printfield{labeltitle} --- \printnames{labelname}%
\addspace\printfield{year}\addspace\printfield[titlecase]{journaltitle}}%
{\multicitedelim}{\usebibmacro{postnote}}

\DeclareCiteCommand{\cites}{\usebibmacro{prenote}}%
{\printfield{labeltitle} --- \printnames{labelname}%
\addspace\printfield{year}\addspace\mkbibbrackets{\usebibmacro{cite}}}%
{\multicitedelim}{\usebibmacro{postnote}}

\DeclareCiteCommand{\citet}{\usebibmacro{prenote}}%
{\printnames{labelname} \addspace\mkbibparens{\printfield{year}}\addspace\mkbibbrackets{\usebibmacro{cite}}}%
{\multicitedelim}{\usebibmacro{postnote}}

%%%%%%%%%%%%%%%%%%%%%%%
% Required for videos %
%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{black}{rgb}{0, 0, 0}
\RequirePackage{media9} %embedding
\RequirePackage[absolute,overlay]{textpos}%full page positioning
%%%%%%%%%%%%%%%%%%%%%%%
% Title page settings %
%%%%%%%%%%%%%%%%%%%%%%%
\setbeamerfont{title}{size=\fontsize{25}{30}}
\setbeamerfont{subtitle}{size=\fontsize{20}{20}}
\setbeamerfont{institute}{size=\fontsize{13}{13}}
\setbeamerfont{deparment}{size=\fontsize{12}{12}}
\setbeamerfont{author}{size=\fontsize{14}{14},series=\bfseries}
\setbeamerfont{email}{size=\fontsize{10}{10}}
\defbeamertemplate*{title page}{fbence_titlepage}
{	{
	\usebeamercolor[fg]{subtitle}
	\vspace{0cm}
	\begin{center}
	\usebeamerfont{title}\inserttitle\par
	\usebeamerfont{subtitle}\insertsubtitle\par
	\end{center}
	}
	\vspace{1cm}

	\begin{columns}[T]
	\begin{column}{4.5cm}
	\hspace{0.5cm}
	\usebeamercolor[fg]{titlegraphic}\inserttitlegraphic
	\end{column}
	\begin{column}{9cm}
	
	\vspace{1cm}
	{\usebeamerfont{author}\insertauthor\par}
	\vspace{0.5cm}
	\usebeamerfont{department}\insertdepartment\par
	\vspace{0.1cm}
	\usebeamerfont{institute}\insertinstitute\par
	\vspace{0.5cm}
	\usebeamerfont{email}\insertemail\par
	%\usebeamerfont{date}\insertdate\par
	\end{column}
  \end{columns}
  {\centering}
}

%%%%%%%%%%%%%%%%%%
% Frame settings %
%%%%%%%%%%%%%%%%%%
\setbeamerfont{chaptertitle}{size={\fontsize{25}{30}}}
\setbeamerfont{frametitle}{size={\fontsize{20}{24}}}
\setbeamerfont{framesubtitle}{size=\fontsize{14}{20}}
\setbeamertemplate{itemize items}[circle]
%%%%%%%%%%%%%%%%%%%%%%%%%%
% Navigation symbols off %
%%%%%%%%%%%%%%%%%%%%%%%%%%
\beamertemplatenavigationsymbolsempty

%%%%%%%%%%%%
% Commands %
%%%%%%%%%%%%

\newcommand{\chaptertitle}[1]{
\begin{frame}[plain]
  \begin{center}
  \usebeamerfont{chaptertitle}
  \usebeamercolor[fg]{frametitle}
  #1
  \end{center}
\end{frame}  
}

\newcommand{\finalslide}[1]{
\begin{frame}[plain]
  \vspace{3cm}
  \begin{center}
  \usebeamerfont{chaptertitle}
  \usebeamercolor[fg]{frametitle}
  #1
  \end{center}
  \vspace{4cm}
{\begin{center}
\usebeamercolor[fg]{frametitle}
\begin{columns}
\begin{column}{4cm}
\insertauthor
\end{column}
\begin{column}{3cm}
\insertemail
\end{column}
\begin{column}{4cm}
\insertwebsite
\end{column}
\end{columns}
\end{center}}
\end{frame}  
}

%department
\def\@department{}
\newcommand{\department}[1]{
  \def\@department{#1}
}
\newcommand{\insertdepartment}{\@department}

%email
\def\@email{}
\newcommand{\email}[1]{
  \def\@email{#1}
}
\newcommand{\insertemail}{\@email}

%website
\def\@website{}
\newcommand{\website}[1]{
  \def\@website{#1}
}
\newcommand{\insertwebsite}{\@website}

% arguments:
% \fullFrameMovie[loop or noloop]{moviename.avi}{posterimage.jpg}{text after like \CopyrightText{my image}}
\newcommand{\fullFrameMovie}[4][loop]
{
    {
        \setbeamercolor{background canvas}{bg=black}


        % to make this work for both horizontally filled and vertically filled images, we create an absolutely
        % positioned textblock* that we force to be the width of the slide.
        % we then place it at (0,0), and then create a box inside of it to ensure that it's always 95% of the vertical
        % height of the frame.  Once we have created an absolutely positioned and sized box, it doesn't matter what
        % goes inside -- it will always be vertically and horizontally centered
        \frame[plain]
        {
            \begin{textblock*}{\paperwidth}(0\paperwidth,0\paperheight)
            \centering
            \vbox to 0.95\paperheight {
                \vfil{
                    \href{run:#2?autostart&#1}{\includegraphics[width=\paperwidth,height=0.95\paperheight,keepaspectratio]{#3}}
                }
                \vfil
            }
            \end{textblock*}
            #4
        }
    }
}

\newcommand{\fullframefigure}[1][]{
\begin{frame}[plain]
        \begin{tikzpicture}[remember picture,overlay]
            \node[at=(current page.center)] {
                \includegraphics[width=\paperwidth]{#1}
            };
        \end{tikzpicture}
     \end{frame}
}

\newcommand\CopyrightText[2][white]{%
 \begin{textblock*}{\paperwidth}(0\paperwidth,.97\paperheight)%
    \hfill\textcolor{#1}{\tiny#2}\hspace{20pt}
  \end{textblock*}
}

